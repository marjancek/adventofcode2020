package aoc20.marjancek.day07

import scala.io.Source
import scala.annotation.tailrec

object HandyHaversacks {

  def main(args: Array[String]): Unit = {
    val rulz = Source.fromFile("./src/aoc20/marjancek/day07/input.txt").getLines
    val mapped = rulz.map(lineToEntry).toMap

    def isColourReachable(from: String, colour: String): Boolean = {
      val reach = mapped.getOrElse(from, Map())
      reach.keySet.contains(colour) || reach.keySet.exists(c => isColourReachable(c, colour))
    }
    val count = mapped.keys.toList.map(c => isColourReachable(c, "shiny gold")).count(b => b)
    println(s"Reachables are : $count")

    def numberSubBags(from: String): Int = {
      val reach = mapped.getOrElse(from, List()).toList
      reach.map({ case (s, i) => i * numberSubBags(s) }).sum + 1
    }
    val total = numberSubBags("shiny gold") - 1
    println(s"Contains : $total")
  }

  val liner = "([a-z]+ [a-z]+) bags contain (.*)\\."r
  def lineToEntry(line: String): (String, Map[String, Int]) = line match {
    case liner(colour, contains) => (colour, contains.split(", ").map(parseEntry).toMap)
  }

  val contains = "([0-9]+) ([a-z]+ [a-z]+) bags?"r
  val end = "(no other bags)"r
  def parseEntry(entry: String): (String, Int) = entry match {
    case contains(num, colour) => (colour, num.toInt)
    case end(finish) => ("", 0)
  }

}