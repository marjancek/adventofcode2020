package aoc20.marjancek.day18

import scala.io.Source
import scala.annotation.tailrec

object OperationOrder {

  sealed trait Node
  trait Op extends Node
  trait Num extends Node
  case class Empty() extends Node
  case class Missing() extends Num
  case class Value(v: Long) extends Num
  case class Expr(form: List[Node]) extends Num
  case class Sum() extends Op
  case class Prod() extends Op
  case class NoOp() extends Op

  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day18/input.txt").getLines.toList

    val values = lines.map(l => parse(l.split("").toList)).map(solve)
    println(values.sum)

    val second = lines.map(l => parse(l.split("").toList)).map(solvePlus)
    println(second.sum)
  }

  // --------------- Parsing ---------------
  def parse(line: List[String], work: Node = Empty()): Node = line match {
    case " " :: xs => parse(xs, work)
    case ("0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9") :: xs =>
      parse(xs, concat(work, Value(line.head.toLong)))
    case "+" :: xs => parse(xs, concat(work, Sum()))
    case "*" :: xs => parse(xs, concat(work, Prod()))
    case "(" :: xs => {
      val nested = extractMatchingParenthesys(xs, List(), 1)
      val solved = Expr(List(parse(nested, Empty())))
      parse(xs.drop(nested.size + 1), concat(work, solved))
    }
    case _ => work
  }

  def concat(pre: Node, pos: Node): Node = (pre, pos) match {
    case (Missing() | Empty(), _) => pos
    case (_, Missing() | Empty()) => pre
    case (Expr(a), _) => Expr(a :+ pos)
    case (_, _) => Expr(List(pre, pos))
  }

  @tailrec
  def extractMatchingParenthesys(form: List[String], acc: List[String], depth: Long): List[String] = form match {
    case "(" :: xs => extractMatchingParenthesys(xs, acc :+ "(", depth + 1)
    case ")" :: xs if (depth > 1) => extractMatchingParenthesys(xs, acc :+ ")", depth - 1)
    case ")" :: xs => acc
    case x :: xs => extractMatchingParenthesys(xs, acc :+ x, depth)
    case _ => acc
  }

  // --------------- Part 1 ---------------
  @tailrec
  def solve(n: Node): Long = reduce(List(n)) match {
    case Value(v) => v
    case n @ _ => solve(n)
  }

  def reduce(nodes: List[Node], left: Num = Value(0), op: Op = NoOp()): Node =
    nodes match {
      case Value(v: Long) :: xs => (op, left) match {
        case (Sum(), Value(b)) => reduce(Value(v + b) +: xs)
        case (Prod(), Value(b)) => reduce(Value(v * b) +: xs)
        case _ => reduce(xs, Value(v))
      }
      case Sum() :: xs => reduce(xs, left, Sum())
      case Prod() :: xs => reduce(xs, left, Prod())
      case Expr(form) :: xs => reduce(reduce(form) +: xs, left, op)
      case _ => left
    }

  // --------------- Part 2 ---------------
  @tailrec
  def solvePlus(n: Node): Long = n match {
    case Value(x) => x
    case Expr(List(Expr(List(single)))) => solvePlus(single)
    case Expr(List(single)) => solvePlus(single)
    case _ => solvePlus(reducePlus(List(n)))
  }

  def reducePlus(nodes: List[Node], left: List[Node] = List(), last: Node = Missing(), op: Op = NoOp()): Node =
    (nodes, op, last) match {
      // values
      case (Value(v: Long) :: xs, Sum(), Value(b)) => reducePlus(xs, left, Value(v + b))
      case (Value(v: Long) :: xs, NoOp(), _) => reducePlus(xs, left, Value(v))
      case (Value(v: Long) :: xs, _, Missing()) => reducePlus(xs, left :+ op, Value(v))
      case (Value(v: Long) :: xs, _, _) => reducePlus(xs, left :+ last :+ op, Value(v))
      // operations
      case (Sum() :: xs, _, _) => reducePlus(xs, left, last, Sum())
      case (Prod() :: xs, _, _) => reducePlus(xs, left, last, Prod())
      // handle expressions without any pluses
      case (Expr(List(Value(x))) :: xs, _, _) => reducePlus(Value(x) +: xs, left, last, op)
      case (Expr(form) :: xs, NoOp(), _) if (noSum(form)) => reducePlus(xs, left :+ Value(solve(Expr(form))))
      case (Expr(form) :: xs, _, Missing()) if (noSum(form)) => reducePlus(xs, left :+ op, Value(solve(Expr(form))))
      case (Expr(form) :: xs, _, _) if (noSum(form)) => reducePlus(xs, left :+ last :+ op, Value(solve(Expr(form))))
      // nested formulas
      case (Expr(form) :: xs, NoOp(), _) => reducePlus(xs, left :+ reducePlus(form), last, op)
      case (Expr(form) :: xs, _, Missing()) => reducePlus(xs, left :+ op, reducePlus(form))
      case (Expr(form) :: xs, _, p @ _) => reducePlus(xs, left :+ last :+ op, reducePlus(form))
      // anything else
      case (h :: xs, _, _) => reducePlus(xs, left :+ h, last, op)
      //terminations
      case (_, _, Missing()) => if (left.length == 1) left.head else Expr(left)
      case (_, NoOp(), _) => Expr(left :+ last)
      case (_) => Expr(left :+ op :+ last)
    }

  def noSum(f: List[Node]): Boolean = f.forall({
    case Sum() => false
    case Expr(f) => noSum(f)
    case _ => true
  })
}