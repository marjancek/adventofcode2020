package aoc20.marjancek.day21

import scala.io.Source
import scala.annotation.tailrec

object AllergenAssessment {
  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day21/input.txt").getLines.toList
    val pairs = lines.map(parse)
    val allergens = pairs.map(_._2).flatten.toSet
    // Check which ingredients appear in ALL those foods labeled for that allergen
    val potentials = allergens.map(a => (a, pairs.filter(p => p._2.contains(a)).map(_._1.toSet)))
      .toMap.mapValues(v => v.tail.foldLeft(v.head)({ case (t, s) => t.intersect(s) }))
    val allergenicIngredients = findAlergens(potentials)
    val bads = allergenicIngredients.map(_._2)

    val count = pairs.map(_._1).flatten.filter(i => (!bads.contains(i))).size
    println(s"Part 1: $count")

    val canonical = allergenicIngredients.sortBy(_._1).map(_._2).mkString((","))
    println(s"Part 2: $canonical")
  }

  @tailrec
  def findAlergens(alergens: Map[String, Set[String]], acc: List[(String, String)] = List()): List[(String, String)] =
    alergens.toList.sortBy(_._2.size) match {
      case (a, is) :: xs if (is.size == 1) => findAlergens(removeIngredient(is.head, alergens), acc :+ (a, is.head))
      case (a, is) :: xs => throw new Exception("Too difficult!")
      case _ => acc
    }

  def removeIngredient(clean: String, alergens: Map[String, Set[String]]): Map[String, Set[String]] =
    alergens.mapValues(_.filter(_ != clean)).filter(_._2.size > 0)

  val line = "([a-z\\s]+) \\(contains ([a-z\\s,]+)\\)"r
  def parse(entry: String): (List[String], List[String]) = entry match {
    case line(ingredients, alergens) => (ingredients.split(" ").toList, alergens.split(",\\s").toList)
    case _ => (List(), List())
  }
}