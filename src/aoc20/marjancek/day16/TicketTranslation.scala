package aoc20.marjancek.day16

import scala.io.Source
import scala.annotation.tailrec

object TicketTranslation {
  type MySparseVector = List[(Int, Double)]
  type Ticket = List[Int]
  case class Span(min: Int, max: Int)
  case class Field(ranges: List[Span])

  def main(args: Array[String]): Unit = {
    val events = Source.fromFile("./src/aoc20/marjancek/day16/input.txt").getLines
    // -- parsing
    val fields = readFields(events, Map())
    events.next() // Skip your ticket: text
    val yourTicket = readTicket(events.next())
    events.next() // Skip blank line
    events.next() // Skip nearby tickets: text
    val nearby = events.map(readTicket).toList
    // -- part 1
    val possibleValids = for (n <- (0 to 999) if fields.values.exists(f => inRange(n, f))) yield n
    val invalids = nearby.flatten.filter(!possibleValids.contains(_))
    println(s"Sum of invalids = ${invalids.sum}")

    // -- part 2
    val allPosiitons = (0 to yourTicket.size - 1)
    val positionVals = nearby.transpose
    val start = fields.keys.map(k => (k, allPosiitons.toList)).toMap
    val (possibles, i) = positionVals.foldLeft((start, 0))({ case ((acc, i), p) => (findAndFilterBads(p, i, acc, fields), i + 1) })
    val redundantless = cleanRedundat(possibles.toList.sortBy(_._2.size), List())
    val departures = redundantless.filter(_._1.startsWith("departure")).flatMap(_._2).map(yourTicket(_)).toList

    print("Product of Departure values = ")
    println(departures.map(_.toLong).fold(1L)(_ * _))
  }

  val rField = "([a-z\\s]+):\\s([0-9]+)-([0-9]+)\\sor\\s([0-9]+)-([0-9]+)"r
  @tailrec
  def readFields(events: Iterator[String], map: Map[String, Field]): Map[String, Field] = events.next() match {
    case rField(field, l1, r1, l2, r2) =>
      readFields(events, map + (field -> Field(List(Span(l1.toInt, r1.toInt), Span(l2.toInt, r2.toInt)))))
    case _ => map
  }

  def readTicket(line: String): List[Int] = line.split(",").map(_.toInt).toList

  def inRange(n: Int, field: Field): Boolean = field.ranges.exists(r => r.max >= n && r.min <= n)

  def incompatibleFields(fields: Map[String, Field], n: Int): List[String] = {
    val r = fields.filter({ case (s, f) => !inRange(n, f) }).map(_._1).toList
    if (r.size == fields.size) List() // all are incompatible? -> it's an outlier; ignore
    else r
  }

  def removeFromFields(valids: Map[String, List[Int]], fields: List[String], n: Int): Map[String, List[Int]] =
    valids.map({
      case (s, v) =>
        if (fields.contains(s)) (s, v.filter(_ != n))
        else (s, v)
    })

  @tailrec
  def findAndFilterBads(pos: List[Int], i: Int, valids: Map[String, List[Int]], fields: Map[String, Field]): Map[String, List[Int]] =
    pos match {
      case x :: xs => findAndFilterBads(xs, i, removeFromFields(valids, incompatibleFields(fields, x), i), fields)
      case _ => valids
    }

  @tailrec
  def cleanRedundat(redundant: List[(String, List[Int])], clean: List[(String, List[Int])]): List[(String, List[Int])] =
    redundant match {
      case (field, valids) :: xs if (valids.size == 1) =>
        cleanRedundat(removeFromAllThenSort(xs, valids.head), (field, valids) +: clean)
      case (field, valids) :: xs if (valids.size == 0) =>
        throw new Exception("ERROR, no solution! " + field)
      case (field, valids) :: xs =>
        throw new Exception("ERROR, multiple solutions! " + field + valids.toString())
      case _ =>
        clean
    }

  def removeFromAllThenSort(redundant: List[(String, List[Int])], n: Int): List[(String, List[Int])] =
    redundant.map({ case (f, v) => (f, v.filter(_ != n)) }).sortBy(_._2.size)

}