package aoc20.marjancek.day19

import scala.io.Source
import scala.annotation.tailrec

object MonsterMessages {

  sealed trait Node
  case class Leaf(c: Char) extends Node
  case class Branch(branches: Set[List[Int]]) extends Node
  type Rules = Map[Int, Node]

  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day19/input.txt").getLines
    val rules = parseRules(lines) // reads until empty line
    val goods = lines.filter(l => isLineGood(l.toList, rules))
    println("Part 1:" + goods.size)

    val lines2 = Source.fromFile("./src/aoc20/marjancek/day19/input2.txt").getLines
    val rules2 = parseRules(lines2) // reads until empty line
    val goods2 = lines2.filter(l => isLineGood(l.toList, rules2))
    println("Part 2:" + goods2.size)
  }

  // ---------- Parse ----------
  @tailrec
  def parseRules(lines: Iterator[String], root: Rules = Map()): Rules =
    if (lines.hasNext) {
      val news = parseLine(lines.next(), root)
      if (news.isEmpty) root // stop on empty line; rules are finished
      else parseRules(lines, root ++ news)
    } else root

  val rule = "([0-9]+): (.+)"r
  val leaf = "([0-9]+): .([a-z])."r
  def parseLine(line: String, root: Rules): Rules = line match {
    case leaf(node, rest) => root + (node.toInt -> Leaf(rest.head))
    case rule(node, rest) => addRule(node.toInt, rest.split(" \\| ").toList, root)
    case _ => Map()
  }

  @tailrec
  def addRule(node: Int, dest: List[String], root: Rules, acc: Set[List[Int]] = Set()): Rules = dest match {
    case x :: xs => addRule(node, xs, root, acc + x.split(" ").toList.map(_.toInt))
    case _ => root + (node -> Branch(acc))
  }

  // ---------------  Verify -----------
  // backtracking: tries by replacing in sequence searching for 'abcd' List[Char]
  // e.g. if [1, 2, 3]  and (1->[[5, 6],[7, 8]]) then try [5, 6, 2, 3] (and also [7, 8, 2, 3])
  // For the first, if (5->a), it will continue matching 'bcd' with [6, 2, 3]
  def isLineGood(line: List[Char], rules: Rules, exp: List[Int] = List(0)): Boolean =
    (line, exp) match {
      case (x :: xs, y :: ys) => rules.get(y) match {
        case Some(Leaf(c)) if (x == c) => isLineGood(xs, rules, ys)
        case Some(Branch(br)) => br.exists(n => isLineGood(line, rules, n ++ ys))
        case _ => false
      }
      case (x :: xs, _) => false
      case (_, y :: ys) => false
      case _ => true
    }
}