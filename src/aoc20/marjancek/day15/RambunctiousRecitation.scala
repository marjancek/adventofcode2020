package aoc20.marjancek.day15

import scala.annotation.tailrec

object RambunctiousRecitation {
  def main(args: Array[String]): Unit = {
    val input: List[Int] = List(1, 20, 8, 12, 0, 14)
    val start = input.zipWithIndex.toMap

    val twentitwentieth = nth(0, start.toMap, input.size, 2020 - 1)
    println(s"2020th=$twentitwentieth")

    val manyth = nth(0, start.toMap, input.size, 30000000 - 1)
    println(s"30000000th=$manyth")
  }

  @tailrec
  def nth(last: Int, map: Map[Int, Int], n: Int, end: Int): Int =
    if (n >= end) last
    else nth(getNext(last, map, n), map + (last -> n), n + 1, end)

  def getNext(last: Int, map: Map[Int, Int], n: Int): Int =
    map.get(last).map(n - _).getOrElse(0)
}