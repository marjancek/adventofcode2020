package aoc20.marjancek.day14

import scala.io.Source
import java.lang.Long
import scala.annotation.tailrec

object DockingData {

  def main(args: Array[String]): Unit = {
    val events = Source.fromFile("./src/aoc20/marjancek/day14/input.txt").getLines.toList
    val mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

    val mem = process1(events, Map(), mask)
    val sum = mem.values.map(n => Long.parseLong(n.reverse, 2)).sum
    println(sum)

    val mem2 = process2(events, Map(), mask)
    val sum2 = mem2.values.map(n => n.toLong).sum
    println(sum2)
    println("\tMemory entries: " + mem2.size)
  }

  val maskr = "mask = ([X01]+)"r
  val memr = "mem\\[([0-9]+)\\] = ([0-9]+)"r

  @tailrec
  def process1(events: List[String], mem: Map[String, String], mask: String): Map[String, String] =
    if (events.isEmpty) mem
    else events.head match {
      case maskr(newMask) => process1(events.tail, mem, newMask.reverse)
      case memr(addr, bits) => process1(events.tail, mem + (addr -> doMask(bits.toLong.toBinaryString.reverse, mask)), mask)
    }

  def doMask(bits: String, mask: String): String =
    bits.zipAll(mask, '0', 'X').map({ case (b, m) => if (m == 'X') b else m }).mkString

  // -----------second-part------------ //

  @tailrec
  def process2(events: List[String], mem: Map[String, String], mask: String): Map[String, String] =
    if (events.isEmpty) mem
    else events.head match {
      case maskr(newMask) => process2(events.tail, mem, newMask.reverse)
      case memr(addr, bits) => {
        var all = calculateAddrs(doMask2(addr, mask)) // expanded addresses
        process2(events.tail, mem ++ all.map(a => (a.toString(), bits)), mask)
      }
    }

  def doMask2(addr: String, mask: String): List[(Char, Char)] =
    addr.toLong.toBinaryString.reverse.zipAll(mask, '0', 'X').toList

  val both = List(List[Char]('1'), List[Char]('0'))
  def calculateAddrs(pairs: List[(Char, Char)]): List[List[Char]] = pairs match {
    case x :: xs => {
      val curr = if (x._2 == 'X') both else if (x._2 == '1') List(List('1')) else List(List(x._1))
      val rest = calculateAddrs(xs)
      if (rest.isEmpty) curr
      else multiply(curr, rest, List())
    }
    case _ => List()
  }

  @tailrec
  def multiply(a: List[List[Char]], b: List[List[Char]], acc: List[List[Char]]): List[List[Char]] =
    if (a.isEmpty) acc
    else multiply(a.tail, b, acc ::: b.map(l => a.head ++ l))
}