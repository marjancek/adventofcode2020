package aoc20.marjancek.day06

import scala.io.Source

object CustomCustoms {

  val valids = ('a' to 'z')

  def main(args: Array[String]): Unit = {
    // dirty glue and split
    val questionaires = Source.fromFile("./src/aoc20/marjancek/day06/input.txt").
      getLines.map(l => if (l.isEmpty()) "--" else l).mkString(" ").split(" -- ")

    println("Q1 : " + questionaires.map(countAnswers).sum)
    println("Q2 : " + questionaires.map(countSameAnswers).sum)
  }

  def countAnswers(group: String): Int =
    group.filter(_ != ' ').distinct.size // count all distinct non-space

  def countSameAnswers(group: String): Int =
    group.split(" ").map(_.toSet) // create set of answers for each person
      .fold(valids.toSet)((a, b) => a.intersect(b)).size // do intersect among all and count
}