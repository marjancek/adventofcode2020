package aoc20.marjancek.day13

import scala.io.Source
import scala.annotation.tailrec

object ShuttleSearch2 {
  def main(args: Array[String]): Unit = {
    val events = Source.fromFile("./src/aoc20/marjancek/day13/input.txt").getLines
    val now = events.next().toLong
    val buses = events.next().split(",").zipWithIndex.filter(_._1 != "x").map(p => (p._1.toLong, dist(p._1.toLong, p._2)))
    println(now, buses.mkString("|"))

    val sol = findSolution(buses.toList, 0, 1)
    println(sol)
  }

  def dist(a: Long, b: Long): Long = (a - (b % a)) % a

  // Find a number that fulfills the mods for all until now.
  // Keep the product of the 'mult' steps, so that we can move forward without changing those mods
  @tailrec
  def findSolution(buses: List[(Long, Long)], acc: Long, mult: Long): Long =
    if (buses.isEmpty) acc
    else findSolution(buses.tail, findCommon(acc, buses.head, mult), mult * buses.head._1)

  // Find the value that that fulfills the mod for the current bus, moving by steps of Jump, which
  // guarantee it won't change the mods for the previous buses
  @tailrec
  def findCommon(acc: Long, bus: (Long, Long), jump: Long): Long =
    if (acc % bus._1 == bus._2) acc
    else findCommon(acc + jump, bus, jump)
}

