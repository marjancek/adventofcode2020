package aoc20.marjancek.day13

import scala.io.Source

object ShuttleSearch {
  def main(args: Array[String]): Unit = {
    val events = Source.fromFile("./src/aoc20/marjancek/day13/input.txt").getLines
    val now = events.next().toInt
    val buses = events.next().split(",").filter(_ != "x").map(_.toInt)
    println(now, buses.mkString("|"))
    val soonest = buses.map(p => (p, nextDeparture(now, p))).sortBy(p => p._2).head
    println(soonest, soonest._1 * (soonest._2 - now))
  }

  def nextDeparture(now: Int, bus: Int): Int = (Math.ceil(1.0 * now / bus) * bus).toInt
}