package aoc20.marjancek.day20

import scala.io.Source
import scala.annotation.tailrec

object JurassicJigsaw {

  type Scan = List[List[Char]]
  type Matches = Map[Int, List[Int]]

  val snake = List( // visible snake 'scales' to search for
    "                  # ",
    "#    ##    ##    ###",
    " #  #  #  #  #  #   ").map(_.toList)

  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day20/input.txt").getLines
    println("Parsing tiles...")
    val images = parse(lines) // get all the scans with their IDs
    println("Calculating neighbours...")
    val borders = allBorders(images.keySet.toList, images)
    val corners = borders.filter(p => p._2.size == 2)
    val walls = borders.filter(p => p._2.size == 3)
    val width = walls.size / 4 + 2
    println(s"Corners:${corners.size}, Walls:${walls.size}, Width:${width}")
    println("Part 1 : " + corners.map(_._1.toLong).fold(1L)(_ * _))

    println("Building entire map...")
    val startId = corners.head._1 // start from any corner
    val start = images.get(startId).get
    val right = images.get(corners.head._2.last).get // has two neighbours
    val down = images.get(corners.head._2.head).get // right / down doesn't matter
    val corner = findOrientation(start, down, right) //  find the correct rotation for the Start
    val cartography = build(corner, startId, images, borders)
    val array = cartography.map(_.toArray).toArray

    println("Searching for snake coordinates...")
    val scales = List.range(0, 7)
      .map(r => rotate(snake, r)) // for each possible snake rotation
      .map(s => s.map(_.toArray).toArray) // convert to array for convenience
      .map(s => findPoints(array, s)) // find the visible 'scales' coordinates
      .foldLeft(Set[(Int, Int)]())(_ ++ _) // gather all coordinates into one single set

    val total = cartography.map(_.count(_ == '#')).sum // total number of #
    val waters = total - scales.size // all # except those that are part of snakes
    println(s"Part 2 : $waters")
  }

  def findPoints(scan: Array[Array[Char]], snake: Array[Array[Char]]): Set[(Int, Int)] = {
    (for (
      x <- 0 to scan.size - snake.size;
      y <- 0 to scan.head.size - snake.head.size;
      val set = findPointsAt(x, y, scan, snake);
      if (set.size == 15)
    ) yield set).flatten.toSet // the snake has 15 visible scales!
  }

  def findPointsAt(x: Int, y: Int, scan: Array[Array[Char]], snake: Array[Array[Char]]): Set[(Int, Int)] = {
    (for (
      i <- 0 to snake.size - 1;
      j <- 0 to snake.head.size - 1;
      if (snake(i)(j) == scan(x + i)(y + j))
    ) yield (x + i, y + j)).toSet
  }

  @tailrec // create entire line, calculate next first tile down, and recurse
  def build(scan: Scan, scanId: Int, images: Map[Int, Scan], borders: Matches, acc: List[List[Scan]] = List()): Scan = {
    val contour = borders.filter(p => (p._2.size < 4))
    val (line, used) = buildRight(scan, scanId, images, contour)
    // find one neighbour out of two, that is not the one on the right that we just used.
    val next = contour.get(scanId).map(l => l.find(p => p != used.tail.head)).flatten
    if (next.isDefined) {
      val newBorders = removeRefs(borders, used)
      val newScan = matchDown(scan, images.get(next.get).get).get
      build(newScan, next.get, images, newBorders, acc :+ line)
    } else // there's no next line! so we're hopefully done: add last line, and glue all together
      (acc :+ line).map(line => line.map(trim) // trim each tile
        .foldLeft(List[List[Char]]())({ case (acc, l) => glueRight(acc, l) })) // glue horizontally
        .flatten // glue vertically
  }

  def trim(scan: Scan): Scan = scan.tail.dropRight(1).map(l => l.tail.dropRight(1))

  def removeRefs(matches: Matches, refs: List[Int]): Matches =
    matches.mapValues(_.filter(r => (!refs.contains(r)))) -- refs

  def buildRight(scan: Scan, img: Int, images: Map[Int, Scan], contour: Matches,
    used: List[Int] = List(), acc: List[Scan] = List()): (List[Scan], List[Int]) =
    contour.get(img).get.map(id => (id, matchRight(scan, images.get(id).get)))
      .filter(_._2.isDefined).map(c => (c._1, c._2.get)) match {
        case x :: xs => // xs should be empty
          buildRight(x._2, x._1, images, removeRefs(contour, used), used :+ img, acc :+ scan)
        case _ => (acc :+ scan, used :+ img)
      }

  def glueRight(scan: Scan, img: Scan): Scan = {
    (scan.transpose ++ img.transpose).transpose
  }

  def findOrientation(corner: Scan, one: Scan, two: Scan): Scan = {
    List.range(0, 8).map(r => rotate(corner, r)).find(p => isCorner(p, one, two)).get
  }

  @tailrec // very naive
  def allBorders(scans: List[Int], all: Map[Int, Scan], acc: Matches = Map()): Matches = scans match {
    case s :: xs => {
      val matches = all.keySet.filter(o => o != s && matchesBorder(all.get(s).get, all.get(o).get))
      allBorders(xs, all, acc + (s -> matches.toList))
    }
    case _ => acc
  }

  def isCorner(corner: Scan, down: Scan, right: Scan): Boolean = {
    matchDown(corner, down).isDefined && matchRight(corner, right).isDefined
  }

  def matchDown(scan: Scan, other: Scan): Option[Scan] = {
    List.range(0, 8).map(r => rotate(other, r)).find(p => scan.last == p.head)
  }

  def matchRight(scan: Scan, other: Scan): Option[Scan] = {
    List.range(0, 8).map(r => rotate(other, r)).find(p => scan.transpose.last == p.transpose.head)
  }

  def matchesBorder(scan: Scan, other: Scan): Boolean = (for (
    n <- (0 to 7); // naive: could use Streams to stop on first hit
    m <- (0 to 7);
    if (rotate(scan, n).last == rotate(other, m).head)
  ) yield true).exists(t => t)

  def rotate[A](m: List[List[A]], count: Int = 1): List[List[A]] = count match {
    case 0 => m
    case 1 => m.transpose.map(_.reverse)
    case 2 => m.transpose.map(_.reverse).transpose.map(_.reverse)
    case 3 => m.transpose.map(_.reverse).transpose.map(_.reverse).transpose.map(_.reverse)
    case 4 => m.transpose
    case 5 => m.transpose.transpose.map(_.reverse)
    case 6 => m.transpose.transpose.map(_.reverse).transpose.map(_.reverse)
    case 7 => m.transpose.transpose.map(_.reverse).transpose.map(_.reverse).transpose.map(_.reverse)
  }

  val title = "Tile ([0-9]+):"r
  @tailrec
  def parse(lines: Iterator[String], images: Map[Int, Scan] = Map()): Map[Int, Scan] =
    if (lines.hasNext) lines.next() match {
      case title(id) => {
        val rows = lines.takeWhile(_ != "").map(_.toList).toList
        parse(lines, images + (id.toInt -> rows))
      }
      case _ => images
    }
    else images
}