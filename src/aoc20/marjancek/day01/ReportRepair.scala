package aoc20.marjancek.day01
import scala.io.Source

object ReportRepair {

  def main(args: Array[String]): Unit = {
    val intset = Source.fromFile("./src/aoc20/marjancek/day01/input1.txt").getLines.map(_.toInt).toSet

    val goodones =
      for (
        a <- intset.toStream;
        val b: Int = 2020 - a;
        if (intset.contains(b))
      ) yield (a, b)

    goodones.headOption match {
      case Some((a, b)) => println(f"Found $a+$b==2020; their product being ${a * b}")
      case None => printf("No match found")
    }
  }

}