package aoc20.marjancek.day01

import scala.io.Source

object ReportRepair2 {
  def main(args: Array[String]): Unit = {
    val intset = Source.fromFile("./src/aoc20/marjancek/day01/input1.txt").getLines.map(_.toInt).toArray

    val goodones = for (
      a <- (0 until intset.size);
      b <- (a until intset.size);
      val c = 2020 - intset.apply(a) - intset.apply(b);
      if (intset.contains(c))
    ) yield (intset.apply(a), intset.apply(b), c)

    goodones.headOption match {
      case Some((a, b, c)) => println(f"Found $a+$b+$c==2020; their product being ${a * b * c}")
      case None => printf("No match found")
    }
  }

}