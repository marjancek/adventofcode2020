package aoc20.marjancek.day24

import scala.io.Source
import scala.annotation.tailrec

object LobbyLayout {
  type BlackSet = Set[Coord]
  case class Coord(x: Int, y: Int, z: Int)

  val directions = Map( // Cube coordinates for Hexagon plane
    ("e" -> Coord(1, -1, 0)),
    ("se" -> Coord(0, -1, 1)),
    ("sw" -> Coord(-1, 0, 1)),
    ("w" -> Coord(-1, 1, 0)),
    ("nw" -> Coord(0, 1, -1)),
    ("ne" -> Coord(1, 0, -1)))

  val initial = Coord(0, 0, 0)

  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day24/input.txt").getLines
    val initial = Coord(0, 0, 0)
    val coords = lines.map(l => parseLine(l, initial)).toList
    val tiling = flipAll(coords)
    println(s"Part 1 : ${tiling.size}")

    val end = List.range(0, 100).foldLeft(tiling)({ case (m, i) => passDay(m) })
    println(s"Part 2 : ${end.size}")
  }

  def passDay(tiling: BlackSet): BlackSet = {
    val minmax = extremes(tiling)
    (for (
      x <- minmax._1.x - 1 to minmax._2.x + 1;
      y <- minmax._1.y - 1 to minmax._2.y + 1;
      val c = Coord(x, y, -x - y); // cube coordinates are such that x+y+z=0
      if (nextIsBlack(c, tiling))
    ) yield c).toSet
  }

  def nextIsBlack(c: Coord, tiling: BlackSet): Boolean =
    (tiling.contains(c), countBros(c, tiling)) match {
      case (false, 2) => true // white with 2 black neighbours
      case (true, 0) => false // black with no black neighbours
      case (true, b) if (b > 2) => false // black with no black neighbours
      case (c, b) => c // otherwise, keep as is
    }

  def extremes(tiling: BlackSet): (Coord, Coord) = {
    val xs = tiling.map(_.x)
    val ys = tiling.map(_.y)
    val zs = tiling.map(_.z)
    (Coord(xs.min, ys.min, zs.min), Coord(xs.max, ys.max, zs.max))
  }

  def countBros(c: Coord, tiling: BlackSet): Int =
    directions.values.map(d => add(c, d)).count(t => tiling.contains(t))

  val step = "(se|sw|ne|nw|e|w)(.*)"r
  @tailrec
  def parseLine(s: String, c: Coord): Coord = s match {
    case step(dir, rest) => parseLine(rest, move(c, dir))
    case _ => c
  }

  @tailrec
  def flipAll(coords: List[Coord], tiling: BlackSet = Set()): BlackSet = coords match {
    case x :: xs if (tiling.contains(x)) => flipAll(xs, tiling - x)
    case x :: xs => flipAll(xs, tiling + x)
    case _ => tiling
  }

  def move(from: Coord, dir: String): Coord = add(from, directions.get(dir).get)

  def add(from: Coord, step: Coord): Coord =
    Coord(from.x + step.x, from.y + step.y, from.z + step.z)
}