package aoc20.marjancek.day11
import scala.io.Source

object SeatingSystem2 {

  def main(args: Array[String]): Unit = {
    val start = Source.fromFile("./src/aoc20/marjancek/day11/input.txt").getLines.toList
    println(findStable(start).map(_.count(_ == '#')).sum)
  }

  case class Point(x: Int, y: Int)

  def firstInDirection(area: List[String], p: Point, dir: Point): Char = {
    val next = Point(p.x + dir.x, p.y + dir.y)
    if (next.x >= 0 && next.y >= 0 && next.x < area.size && (next.y < area.head.size)) {
      if (area(next.x)(next.y) != '.')
        area(next.x)(next.y)
      else
        firstInDirection(area, next, dir)
    } else '.'
  }

  val directions = List(Point(-1, -1), Point(-1, 0), Point(-1, 1), Point(0, -1), Point(0, 1), Point(1, -1), Point(1, 0), Point(1, 1))
  def flipSeat(area: List[String], i: Int, j: Int): Char = {
    val neigh = directions.map(d => firstInDirection(area, Point(i, j), d))
    if (area(i)(j) == '#' && neigh.count(_ == '#') >= 5) 'L'
    else if (area(i)(j) == 'L' && !neigh.contains('#')) '#'
    else area(i)(j)
  }

  def nextState(area: List[String]): Seq[String] = for (i <- 0 until area.length) yield (nextLine(area, i))

  def nextLine(area: List[String], i: Int): String = (for (j <- 0 until area(i).size) yield flipSeat(area, i, j)).mkString

  def findStable(area: List[String]): List[String] = {
    val newArea = nextState(area).toList
    if (newArea == area) area
    else findStable(newArea)
  }

}