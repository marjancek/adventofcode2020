package aoc20.marjancek.day11
import scala.io.Source

object SeatingSystem {

  def main(args: Array[String]): Unit = {
    val start = Source.fromFile("./src/aoc20/marjancek/day11/input.txt").getLines.toList
    println(findStable(start).map(_.count(_ == '#')).sum)
  }

  def flipSeat(area: List[String], i: Int, j: Int): Char = {
    val neighbours = for (
      a <- ((i - 1) to (i + 1));
      b <- ((j - 1) to (j + 1));
      if (a >= 0 && b >= 0 && a < area.size && b < area.head.length && (i != a || j != b))
    ) yield area(a)(b)

    if (area(i)(j) == '#' && neighbours.count(_ == '#') >= 4) 'L'
    else if (area(i)(j) == 'L' && !neighbours.contains('#')) '#'
    else area(i)(j)
  }

  def nextState(area: List[String]): Seq[String] = for (i <- 0 until area.length) yield (nextLine(area, i))

  def nextLine(area: List[String], i: Int): String = (for (j <- 0 until area(i).size) yield flipSeat(area, i, j)).mkString

  def findStable(area: List[String]): List[String] = {
    val newArea = nextState(area).toList
    if (newArea == area) area
    else findStable(newArea)
  }

}