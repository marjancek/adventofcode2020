package aoc20.marjancek.day17

import scala.io.Source

object ConwayCubes {
  case class Coord(x: Int, y: Int, z: Int)
  type Space = Map[Coord, Char]

  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day17/input.txt").getLines.toList
    val size = (lines.head.length() - 1) / 2
    val start = lines.zipWithIndex.foldLeft(Map[Coord, Char]())((m, ln) => parse(ln._1, ln._2 - size, size, m))
    val end = List.range(size + 1, size + 6 + 1).foldLeft(start)((m, i) => conway(m, i))
    println(end.values.count(_ == '#'))
  }

  def parse(line: String, row: Int, width: Int, map: Space): Space =
    map ++ line.zipWithIndex.map({ case (c, i) => (Coord(row, i - width, 0), c) }).toMap

  def conway(map: Space, i: Int): Space = (
    for (
      a <- (-i to i);
      b <- (-i to i);
      c <- (-i to i);
      val pos = Coord(a, b, c);
      val cube = map.get(pos).getOrElse('.');
      val nActives = getNeighbours(map, pos).count(_ == '#')
    ) yield (pos -> ConwayCubes.flipCube(cube, nActives))).toMap

  def getNeighbours(map: Space, coord: Coord): List[Char] = (
    for (
      a <- ((coord.x - 1) to (coord.x + 1));
      b <- ((coord.y - 1) to (coord.y + 1));
      c <- ((coord.z - 1) to (coord.z + 1));
      if (coord.x != a || coord.y != b || coord.z != c)
    ) yield map.get(Coord(a, b, c)).getOrElse('.')).toList

  def flipCube(c: Char, nActives: Int): Char =
    if (c == '.' && nActives == 3) '#'
    else if (c == '#' && (nActives == 2 || nActives == 3)) '#'
    else '.'

}