package aoc20.marjancek.day04

import scala.io.Source

object PassportProcessing2 {
  val valids = List("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")

  def main(args: Array[String]): Unit = {
    // dirty glue and split
    val passports = Source.fromFile("./src/aoc20/marjancek/day04/input.txt").
      getLines.map(l => if (l.isEmpty()) "--" else l).mkString(" ").split(" -- ")
    val goods = passports.map(passportValid).count(b => b)
    println(s"Found $goods passports")
  }

  def passportValid(p: String): Boolean = {
    val found = p.split(" ");
    PassportProcessing.passportValid(p) && found.forall(entryValid)
  }

  val byr = "byr:([0-9]{4})"r
  val iyr = "iyr:(20[0-9]{2})"r
  val eyr = "eyr:(20[0-9]{2})"r
  val hgt = "hgt:([0-9]+)(in|cm)"r
  val hcl = "hcl:#([0-9a-f]{6})"r
  val ecl = "ecl:(amb|blu|brn|gry|grn|hzl|oth)"r
  val pid = "pid:([0-9a-f]{9})"r
  val cid = "cid:(.*)"r

  def entryValid(s: String): Boolean = s match {
    case byr(year) => year.toInt >= 1920 && year.toInt <= 2002
    case iyr(year) => year.toInt >= 2010 && year.toInt <= 2020
    case eyr(year) => year.toInt >= 2020 && year.toInt <= 2030
    case hgt(num, unit) =>
      if (unit == "cm") num.toInt >= 150 && num.toInt <= 193
      else if (unit == "in") num.toInt >= 59 && num.toInt <= 76
      else false
    case hcl(code) => true
    case ecl(color) => true
    case pid(num) => true
    case cid(num) => true
    case _ => false
  }
}