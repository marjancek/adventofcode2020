package aoc20.marjancek.day04

import scala.io.Source

object PassportProcessing {

  def main(args: Array[String]): Unit = {
    // dirty glue and split
    val passports = Source.fromFile("./src/aoc20/marjancek/day04/input.txt").
      getLines.map(l => if (l.isEmpty()) "--" else l).mkString(" ").split(" -- ")
    val goods = passports.map(passportValid).count(b => b)
    println(s"Found $goods passports")
  }

  val valids = List("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")
  def passportValid(p: String): Boolean = {
    val found = p.split("[: ]");
    valids.forall(p => found.contains(p))
  }
}