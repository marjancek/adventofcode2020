package aoc20.marjancek.day03

import scala.io.Source
import scala.annotation.tailrec

object TobogganTrajectory2 {

  def main(args: Array[String]): Unit = {
    val area = Source.fromFile("./src/aoc20/marjancek/day03/input.txt").getLines.toList
    val slopes = List((1, 1), (3, 1), (5, 1), (7, 1), (1, 2))
    val m = slopes.map(s => TobogganTrajectory.countTrees(0, 0, area, s, 0)).fold(1)(_ * _)
    println(m)
  }

}