package aoc20.marjancek.day03

import scala.io.Source
import scala.annotation.tailrec

object TobogganTrajectory {

  def main(args: Array[String]): Unit = {
    val area = Source.fromFile("./src/aoc20/marjancek/day03/input.txt").getLines.toList
    println(countTrees(0, 0, area, (3, 1), 0))
  }

  @tailrec
  def countTrees(x: Int, y: Int, area: List[String], slope: (Int, Int), count: Int): Int =
    if (y >= area.size)
      count
    else if (area(y)(x) == '#')
      countTrees((x + slope._1) % area.head.size, y + slope._2, area, slope, count + 1)
    else
      countTrees((x + slope._1) % area.head.size, y + slope._2, area, slope, count)
}