package aoc20.marjancek.day23

import scala.annotation.tailrec

object CrabCups {

  type Circle = Map[Int, Int]
  case class State(circle: Circle, first: Int, last: Int)

  def main(args: Array[String]): Unit = {
    val inputa = "389125467"
    val inputb = "792845136"
    val input = inputb.split("").map(_.toInt).toList

    val state = read(input, input.head)
    val game1 = game(100, state)
    val start = game1.circle.get(1).get
    print("Part 1: ")
    display(start, game1.circle)(game1.circle.size - 1)

    val extended = fill(1000000, state)
    val game2 = game(10000000, extended)
    val first = game2.circle.get(1).get
    val second = game2.circle.get(first).get
    val score = first.toLong * second.toLong
    println(s"Part 2: $score")
  }

  @tailrec
  def read(input: List[Int], first: Int, acc: Circle = Map()): State = input match {
    case x :: y :: rest => read(y :: rest, first, acc + (x -> y))
    case y :: rest => State(acc + (y -> first), first, y)
    case _ => throw new Exception("Empty list to read")
  }

  @tailrec
  def game(n: Int, state: State): State =
    if (n > 0) game(n - 1, move(state))
    else state

  def move(state: State): State = {
    val first = state.first
    val second = state.circle.get(first).get
    val third = state.circle.get(second).get
    val forth = state.circle.get(third).get
    val fifth = state.circle.get(forth).get
    val pivot = choosePivot(first, Set(first, second, third, forth), state.circle.size)
    val after = state.circle.get(pivot).get

    State(state.circle + (pivot -> second) + (forth -> after) + (first -> fifth), fifth, first)
  }

  @tailrec
  def choosePivot(n: Int, vals: Set[Int], max: Int): Int =
    if (!vals.contains(n)) n
    else n match {
      case 1 => choosePivot(max, vals, max)
      case _ => choosePivot(n - 1, vals, max)
    }

  def fill(n: Int, state: State): State = {
    val from = state.circle.size + 1
    val missing = for (x <- (from to n)) yield (x -> (x + 1))
    State(state.circle ++ missing + (state.last -> from) + (n -> state.first), state.first, n)
  }

  @tailrec
  def display(first: Int, map: Circle)(n: Int = map.size): Unit = n match {
    case 1 => println(first)
    case _ => { print(first); display(map.get(first).get, map)(n - 1) }
  }

}