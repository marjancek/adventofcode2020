package aoc20.marjancek.day22

import scala.io.Source

object CrabCombat {
  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day22/input.txt").getLines
    lines.next() // skip player name
    val p1 = lines.takeWhile(_ != "").map(_.toInt).toList
    lines.next() // skip player name
    val p2 = lines.takeWhile(_ != "").map(_.toInt).toList

    val (winner, deck) = combat(p1, p2)
    val score1 = score(deck)
    println(s"P$winner score: $score1")

    val (winner2, deck2) = recursive(p1, p2)
    val score2 = score(deck2)
    println(s"P$winner2 score: $score2")
  }

  def score(deck: List[Int]): Int = deck.reverse.zipWithIndex.map(p => p._1 * (p._2 + 1)).sum

  def combat(p1: List[Int], p2: List[Int]): (Int, List[Int]) = (p1, p2) match {
    case (x :: xs, y :: ys) => if (x > y) combat(xs :+ x :+ y, ys) else combat(xs, ys :+ y :+ x)
    case _ => if (p1.size > 0) (1, p1) else (2, p2)
  }

  def recursive(p1: List[Int], p2: List[Int], delt: Set[(List[Int], List[Int])] = Set()): (Int, List[Int]) =
    if (delt.contains((p1, p2))) (1, p1) // if repeated: P1 wins
    else (p1, p2) match {
      case (x :: xs, y :: ys) => {
        val winner = // if we can: play recursivelly; other wise, biggest wins
          if (x <= xs.length && y <= ys.length) recursive(xs.take(x), ys.take(y))._1
          else if (x > y) 1
          else 2
        // give gards to winner, store hand, play again
        if (winner == 1) recursive(xs :+ x :+ y, ys, delt + ((p1, p2)))
        else recursive(xs, ys :+ y :+ x, delt + ((p1, p2)))
      }
      case _ => if (p1.size > 0) (1, p1) else (2, p2) // one run out of cards => end of game
    }

}