package aoc20.marjancek.day25

import scala.io.Source
import scala.annotation.tailrec

object ComboBreaker {
  type Pair = (Long, Long) // public, private crypto key pair

  def main(args: Array[String]): Unit = {
    val publics = Source.fromFile("./src/aoc20/marjancek/day25/input.txt").getLines.map(_.toLong).toSet
    println(publics)

    val found = search(publics)
    found.foreach(println)
    val handshake = loop(found.head._1, found.last._2)
    println(s"Part 1 : $handshake")
  }

  def search(publics: Set[Long], found: Set[Pair] = Set(), value: Long = 1L, times: Long = 1L): Set[Pair] =
    if (publics.isEmpty) found
    else {
      val next = encript(7, value)
      val matches = publics.filter(_ == next)
      val newones = matches.map(pub => (pub, times))
      search(publics -- matches, found ++ newones, next, times + 1)
    }

  @tailrec
  def loop(subject: Long, times: Long, value: Long = 1L): Long =
    if (times > 0) loop(subject, times - 1, encript(subject, value))
    else value

  def encript(subject: Long, value: Long = 1L): Long = (value * subject) % 20201227L
}