package aoc20.marjancek.day10

import scala.io.Source
import scala.annotation.tailrec

object AdapterArray {

  def main(args: Array[String]): Unit = {
    val adapters = Source.fromFile("./src/aoc20/marjancek/day10/input.txt").getLines.map(_.toLong).toList

    val sorted = (adapters :+ (adapters.max + 3)).sorted
    val diffs = sorted.foldLeft((List[Long](), 0L))((p, n) => (p._1 :+ (n - p._2), n))._1
    val ones = diffs.count(_ == 1)
    val threes = diffs.count(_ == 3)

    println(ones, threes, ones * threes)

    val combinations = countDynamic(List(new AdapterCount(0, 1)), sorted)
    println(combinations)
  }

  case class AdapterCount(adapter: Long, count: Long)

  @tailrec
  def countDynamic(counts: List[AdapterCount], rest: List[Long]): Long =
    if (rest.isEmpty) counts.head.count
    else {
      val newCounts = counts.takeWhile(_.adapter + 3 >= rest.head) // remove further than 3
      val newEntry = new AdapterCount(rest.head, newCounts.map(_.count).sum)
      countDynamic(newEntry +: counts, rest.tail)
    }
}