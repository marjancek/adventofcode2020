package aoc20.marjancek.day05

import scala.io.Source

object BinaryBoarding {
  def main(args: Array[String]): Unit = {
    val area = Source.fromFile("./src/aoc20/marjancek/day05/input.txt").getLines.toList
    val ids = area.map(rowToId)
    val highest = ids.max
    val missings = for (
      s <- (0 to highest);
      if (!ids.contains(s) && ids.contains(s - 1) && ids.contains(s + 1))
    ) yield s

    print(s"The highest seat id is $highest")
    print(", yours is " + missings.headOption.getOrElse(-1))
    println("; have a pleasent flight.")
  }

  def rowToId(c: String): Int = {
    val binary = c.map(charToBin)
    val row = binary.take(7).foldLeft(0)((acc, b) => (acc << 1) + b)
    val col = binary.drop(7).foldLeft(0)((acc, b) => (acc << 1) + b)
    row * 8 + col
  }

  def charToBin(c: Char): Int = c match {
    case 'F' | 'L' => 0
    case 'B' | 'R' => 1
    case _ => throw new Exception(s"donnow '$c'")
  }
}