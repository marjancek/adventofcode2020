package aoc20.marjancek.day12

import scala.io.Source

object RainRisk {
  def main(args: Array[String]): Unit = {
    val events = Source.fromFile("./src/aoc20/marjancek/day12/input.txt").getLines.toList

    val start = new State(new Vect(0, 0), new Vect(-1, 0))
    val result = events.foldLeft(start)((s, line) => s.processEvent(line))
    println(result.pos.manhatam)

    val begin = new WayState(new Vect(0, 0), new Vect(-10, 1))
    val second = events.foldLeft(begin)((s, line) => s.processEvent(line))
    println(second.ship.manhatam)

  }

  class Vect(val hor: Int, val ver: Int) {
    def +(other: Vect): Vect = new Vect(hor + other.hor, ver + other.ver)
    def *(v: Int): Vect = new Vect(hor * v, ver * v)
    def moveSN(n: Int): Vect = new Vect(hor, ver + n)
    def moveEW(n: Int): Vect = new Vect(hor + n, ver)
    def rotateLeft(n: Int): Vect = if (n % 4 <= 0) this else (new Vect(ver, -hor)).rotateLeft(n - 1)
    def rotateRight(n: Int): Vect = if (n % 4 <= 0) this else (new Vect(-ver, hor)).rotateRight(n - 1)
    def manhatam: Int = Math.abs(hor) + Math.abs(ver)
  }

  val parser = "(N|S|E|W|L|R|F)([0-9]+)"r

  class State(val pos: Vect, val dir: Vect) {
    def processEvent(line: String): State = line match {
      case parser("N", n) => new State(pos.moveSN(n.toInt), dir)
      case parser("S", n) => new State(pos.moveSN(-(n.toInt)), dir)
      case parser("W", n) => new State(pos.moveEW(n.toInt), dir)
      case parser("E", n) => new State(pos.moveEW(-(n.toInt)), dir)
      case parser("L", n) => new State(pos, dir.rotateLeft(n.toInt / 90))
      case parser("R", n) => new State(pos, dir.rotateRight(n.toInt / 90))
      case parser("F", n) => new State(pos + (dir * n.toInt), dir)
    }
  }

  class WayState(val ship: Vect, val wayp: Vect) {
    def processEvent(line: String): WayState = line match {
      case parser("N", n) => new WayState(ship, wayp.moveSN(n.toInt))
      case parser("S", n) => new WayState(ship, wayp.moveSN(-(n.toInt)))
      case parser("W", n) => new WayState(ship, wayp.moveEW(n.toInt))
      case parser("E", n) => new WayState(ship, wayp.moveEW(-(n.toInt)))
      case parser("L", n) => new WayState(ship, wayp.rotateLeft(n.toInt / 90))
      case parser("R", n) => new WayState(ship, wayp.rotateRight(n.toInt / 90))
      case parser("F", n) => new WayState(ship + (wayp * n.toInt), wayp)
    }
  }

}