package aoc20.marjancek.day08
import scala.io.Source

object HandheldHalting {
  type State = (Int, Int) // accumulator, program_counter
  type Instruction = (String, Int) // inst, val

  def main(args: Array[String]): Unit = {
    val program = Source.fromFile("./src/aoc20/marjancek/day08/input.txt").getLines
      .map(parseInstruction).toArray

    println("Q1: " + runButStopIfRepeated(program, (0, 0), Set()))

    val finalized = generateAlternatives(program)
      .map(p => runButStopIfRepeated(p, (0, 0), Set()))
      .find(_.isInstanceOf[Right[Int, Int]])

    println("Q2: " + finalized)
  }

  // runs program until either program counter in repeated position (Left) or in last position (Right)
  def runButStopIfRepeated(prg: Array[Instruction], s: State, visited: Set[Int]): Either[Int, Int] =
    if (visited.contains(s._2)) Left(s._1) // we've already executed this instruction >:|
    else {
      val ins = prg(s._2)
      val newState = applyInstruction(s, ins)
      if (s._2 + 1 == prg.length) // just executed last instruction :)
        Right(newState._1)
      else
        runButStopIfRepeated(prg, newState, visited + s._2)
    }

  val instruct = "(nop|acc|jmp) ([-|+][0-9]+)"r
  def parseInstruction(line: String): Instruction = line match {
    case instruct(op, num) => (op, num.toInt)
  }

  def applyInstruction(s: State, ins: Instruction): State = ins match {
    case ("jmp", step) => (s._1, s._2 + step)
    case ("acc", value) => (s._1 + value, s._2 + 1)
    case ("nop", _) => (s._1, s._2 + 1)
    case _ => throw new Exception(s"Uknonwn instruction $ins")
  }

  // generate iterable of all possible one-instruction-changed programs
  def generateAlternatives(prg: Array[Instruction]): Iterable[Array[Instruction]] = {
    for (i <- (0 to prg.length - 1); if (prg(i)._1 != "acc")) yield (prg.take(i) :+ swapIns(prg(i))) ++ prg.drop(i + 1)
  }

  def swapIns(ins: Instruction): Instruction = ins match {
    case ("jmp", step) => ("nop", step)
    case ("nop", step) => ("jmp", step)
    case in => in
  }
}