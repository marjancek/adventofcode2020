package aoc20.marjancek.day09
import scala.io.Source
import scala.annotation.tailrec

object EncodingError {
  def main(args: Array[String]): Unit = {
    val nums = Source.fromFile("./src/aoc20/marjancek/day09/input.txt").getLines.map(_.toLong).toList
    val (preamble, rest) = nums.splitAt(25)

    val alien = findUnpaired(preamble, rest)
    println(s"Unpairable:$alien")

    val search = findListSums(alien.get, nums).getOrElse(List(-1L))
    println("Found : " + search.min + "+" + search.max + " = " + (search.min + search.max))
  }

  @tailrec
  def findUnpaired(pre: List[Long], rest: List[Long]): Option[Long] = rest match {
    case next :: tail => if (cantPair(pre, next).isEmpty) Some(next)
    else findUnpaired(pre.tail :+ next, tail)
    case _ => None
  }

  def cantPair(pre: List[Long], num: Long): List[Long] =
    for (n <- pre; if (pre.contains(num - n))) yield n

  @tailrec
  def findListSums(num: Long, seq: List[Long]): Option[List[Long]] = searchSum(num, seq) match {
    case Some((_, ndx)) => Some(seq.take(ndx + 1))
    case None if (!seq.isEmpty) => findListSums(num, seq.tail)
    case _ => None
  }

  def searchSum(num: Long, seq: List[Long]): Option[(Long, Int)] =
    seq.toStream.zipWithIndex
      .scanLeft((0L, -1))({ case ((a, i), (n, j)) => (a + n, j) })
      .filter(p => (p._1) == num).headOption

}