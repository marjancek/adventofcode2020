package aoc20.marjancek.day02

import scala.io.Source

object PasswordPhilosophy2 {
  val entry = "([0-9]+)\\-([0-9]+) ([a-z])\\: ([a-z]+)"r

  def validEntry(line: String): Boolean = line match {
    case entry(f, s, l, p) => (p.charAt(f.toInt - 1) == l.charAt(0)) != (p.charAt(s.toInt - 1) == l.charAt(0))
    case _ => false
  }

  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day02/input.txt").getLines
    val goods: Int = lines.map(validEntry).count(a => a)
    println(s"found $goods valid passwords")
  }
}