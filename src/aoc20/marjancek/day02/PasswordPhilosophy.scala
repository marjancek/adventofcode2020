package aoc20.marjancek.day02

import scala.io.Source

object PasswordPhilosophy {
  val entry = "([0-9]+)\\-([0-9]+) ([a-z])\\: ([a-z]+)"r

  def validEntry(line: String): Boolean = line match {
    case entry(low, high, letter, pass) => {
      val count = pass.split("").filter(_ == letter).length
      count >= low.toInt && count <= high.toInt
    }
    case _ => false
  }

  def main(args: Array[String]): Unit = {
    val lines = Source.fromFile("./src/aoc20/marjancek/day02/input.txt").getLines
    val goods: Int = lines.map(validEntry).count(a => a)
    println(s"found $goods valid passwords")
  }
}